/* eslint-disable no-console */
const express = require('express');
const bodyParser = require('body-parser');

const actorRoutes = require('./routes/actor');
const filmRoutes = require('./routes/film');
const genreRoutes = require('./routes/genre');

const checkApiKey = () => (req, res, next) => {
    if (req.headers.authorization !== 'Bearer 8f94826adab8ffebbeadb4f9e161b2dc') {
        res.status(401).send('Server requires authorization');
    } else {
        next();
    }
};

const app = express();
app.use(checkApiKey());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const HTTP_PORT = 8000;

app.listen(HTTP_PORT, () => {
    console.log(`Server running on port ${HTTP_PORT}`);
});

// Basic route
app.get('/', (req, res) => {
    res.json({ message: 'Hello World' });
});

// Routes "Actor"
app.use('/api/actor', actorRoutes);

// Routes "Film"
app.use('/api/film', filmRoutes);

// Routes "Genre"
app.use('/api/genre', genreRoutes);

// Fallback route
app.use((req, res) => {
    res.status(404);
});

const md5 = require('md5');
const db = require('../database');
const GenreRepository = require('../repository/GenreRepository');
const FilmRepository = require('../repository/FilmRepository');

exports.genre_list = (req, res) => {
    const repo = new GenreRepository(db);
    repo.list()
        .then((result) => {
            res.json({
                success: true,
                data: result,
            });
        })
        .catch((err) => {
            res.status(500).json({ error: err.message });
        });
};

exports.genre_get = (req, res) => {
    const repo = new GenreRepository(db);
    repo.get(req.params.id)
        .then((result) => {
            const etag = md5(JSON.stringify(result));
            res.set('ETag', etag);

            res.json({
                success: true,
                data: result,
            });
        })
        .catch((err) => {
            res.status(404).json({ error: err.message });
        });
};

exports.genre_create = (req, res) => {
    const errors = [];
    ['name'].forEach((field) => {
        if (!req.body[field]) {
            errors.push(`Field '${field}' is missing from request body`);
        }
    });
    if (errors.length) {
        res.status(400).json({
            success: false,
            errors,
        });
        return;
    }

    const repo = new GenreRepository(db);

    repo.create({
        name: req.body.name,
    })
        .then((result) => {
            res
                .status(201)
                .json({
                    success: true,
                    id: result,
                });
        })
        .catch((err) => {
            res.status(400).json({ error: err.message });
        });
};

exports.genre_update = async (req, res) => {
    const repo = new GenreRepository(db);
    let etag = '';

    await repo.get(req.params.id)
        .then((result) => {
            etag = md5(JSON.stringify(result));
        })
        .catch((err) => {
            res.status(404).json({ error: err.message });
        });

    if (req.headers['if-match'] !== etag) {
        res.status(412).send('ETag non valide');
    } else {
        const errors = [];
        ['name'].forEach((field) => {
            if (!req.body[field]) {
                errors.push(`Field '${field}' is missing from request body`);
            }
        });
        if (errors.length) {
            res.status(400).json({
                success: false,
                errors,
            });
            return;
        }

        repo.update(
            req.params.id,
            {
                name: req.body.name,
            },
        )
            .then(() => {
                repo.get(req.params.id)
                    .then((result) => {
                        res.json({
                            success: true,
                            data: result,
                        });
                    });
            })
            .catch((err) => {
                res.status(400).json({ error: err.message });
            });
    }
};

exports.genre_delete = (req, res) => {
    const repo = new GenreRepository(db);
    const repoFilm = new FilmRepository(db);

    repoFilm.listGenre(req.params.id)
        .then((result) => {
            if (result.length > 0) {
                res.status(400).json({ error: "Il est impossible de supprimer le genre puisqu'il est utilisé dans un ou plusieurs films" });
            } else {
                repo.delete(req.params.id)
                    .then(() => {
                        res.status(204)
                            .json({
                                success: true,
                            });
                    })
                    .catch((err) => {
                        res.status(400).json({ error: err.message });
                    });
            }
        })
        .catch((err) => {
            res.status(500).json({ error: err.message });
        });
};

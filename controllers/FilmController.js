const md5 = require('md5');
const db = require('../database');
const FilmRepository = require('../repository/FilmRepository');
const FilmActorRepository = require('../repository/FilmActorRepository');
const GenreRepository = require('../repository/GenreRepository');
const ActorRepository = require('../repository/ActorRepository');

exports.film_list = (req, res) => {
    const repo = new FilmRepository(db);
    const repoFilmActor = new FilmActorRepository(db);
    const repoGenre = new GenreRepository(db);
    const repoActor = new ActorRepository(db);

    const listFilmActorGenre = [];
    let Genre = {};
    let Actor = {};

    repo.list()
        .then(async (filmList) => {
            await Promise.all(filmList.map(async (film) => {
                await repoGenre.get(film.genre_id)
                    .then((genre) => {
                        Genre = genre;
                    })
                    .catch((err) => {
                        res.status(400).json({ error: err.message });
                    });

                await repoFilmActor.listFilm(film.id)
                    .then(async (filmActorList) => {
                        const ActorList = [];

                        await Promise.all(filmActorList.map(async (filmActor) => {
                            await repoActor.get(filmActor.actor_id)
                                .then((actor) => {
                                    ActorList.push(actor);
                                    return ActorList;
                                })
                                .catch((err) => {
                                    res.status(400).json({ error: err.message });
                                });
                        }));

                        Actor = ActorList;
                        return Actor;
                    })
                    .catch((err) => {
                        res.status(400).json({ error: err.message });
                    });

                listFilmActorGenre.push({
                    ...film, genre: { ...Genre }, actor: [...Actor],
                });
                return listFilmActorGenre;
            }));

            res.json({
                success: true,
                data: listFilmActorGenre,
            });
        })
        .catch((err) => {
            res.status(500).json({ error: err.message });
        });
};

exports.film_get = (req, res) => {
    const repo = new FilmRepository(db);
    const repoFilmActor = new FilmActorRepository(db);
    const repoGenre = new GenreRepository(db);
    const repoActor = new ActorRepository(db);

    const listFilmActorGenre = [];
    let Genre = {};
    let Actor = {};

    repo.get(req.params.id)
        .then(async (film) => {
            const etag = md5(JSON.stringify(film));
            res.set('ETag', etag);

            await repoGenre.get(film.genre_id)
                .then((genre) => {
                    Genre = genre;
                })
                .catch((err) => {
                    res.status(400).json({ error: err.message });
                });

            await repoFilmActor.listFilm(film.id)
                .then(async (filmActorList) => {
                    const ActorList = [];

                    await Promise.all(filmActorList.map(async (filmActor) => {
                        await repoActor.get(filmActor.actor_id)
                            .then((actor) => {
                                ActorList.push(actor);
                                return ActorList;
                            })
                            .catch((err) => {
                                res.status(400).json({ error: err.message });
                            });
                    }));

                    Actor = ActorList;
                    return Actor;
                })
                .catch((err) => {
                    res.status(400).json({ error: err.message });
                });

            listFilmActorGenre.push({
                ...film, genre: { ...Genre }, actor: [...Actor],
            });

            res.json({
                success: true,
                data: listFilmActorGenre,
            });
        })
        .catch((err) => {
            res.status(500).json({ error: err.message });
        });
};

exports.film_create = async (req, res) => {
    const repo = new FilmRepository(db);
    const repoFilmActor = new FilmActorRepository(db);
    const repoGenre = new GenreRepository(db);
    const repoActor = new ActorRepository(db);

    const errors = [];
    ['name', 'synopsis', 'release_year', 'genre_id', 'actor_id'].forEach((field) => {
        if (!req.body[field]) {
            errors.push(`Field '${field}' is missing from request body`);
        }
    });

    if (req.body.genre_id) {
        await repoGenre.count(req.body.genre_id)
            .then((genre) => {
                if (genre['COUNT(*)'] <= 0) {
                    errors.push(`Il est impossible de créer le film puisque le genre '${req.body.genre_id}' n'existe pas`);
                }
            })
            .catch((err) => {
                res.status(500).json({ error: err.message });
            });
    }

    if (req.body.actor_id) {
        await Promise.all(req.body.actor_id.map(async (idActor) => {
            await repoActor.count(idActor)
                .then((actor) => {
                    if (actor['COUNT(*)'] <= 0) {
                        errors.push(`Il est impossible de créer le film puisque l'acteur '${idActor}' n'existe pas`);
                    }
                })
                .catch((err) => {
                    res.status(500).json({ error: err.message });
                });
        }));
    }

    if (errors.length) {
        res.status(400).json({
            success: false,
            errors,
        });
        return;
    }

    let resultCreate = {};

    await repo.create({
        name: req.body.name,
        synopsis: req.body.synopsis,
        release_year: req.body.release_year,
        genre_id: req.body.genre_id,
    })
        .then((result) => {
            resultCreate = result;
        })
        .catch((err) => {
            res.status(400).json({ error: err.message });
        });

    await repo.getLastID()
        .then(async (lastID) => {
            await Promise.all(req.body.actor_id.map(async (idActor) => {
                await repoFilmActor.create({
                    film_id: lastID['MAX(id)'],
                    actor_id: idActor,
                })
                    .catch((err) => {
                        res.status(400).json({ error: err.message });
                    });
            }));

            res
                .status(201)
                .json({
                    success: true,
                    data: resultCreate,
                });
        })
        .catch((err) => {
            res.status(400).json({ error: err.message });
        });
};

exports.film_update = async (req, res) => {
    const repo = new FilmRepository(db);
    let etag = '';

    await repo.get(req.params.id)
        .then((result) => {
            etag = md5(JSON.stringify(result));
        })
        .catch((err) => {
            res.status(404).json({ error: err.message });
        });

    if (req.headers['if-match'] !== etag) {
        res.status(412).send('ETag non valide');
    } else {
        const repoFilmActor = new FilmActorRepository(db);
        const repoGenre = new GenreRepository(db);
        const repoActor = new ActorRepository(db);

        const errors = [];
        ['name', 'synopsis', 'release_year', 'genre_id', 'actor_id'].forEach((field) => {
            if (!req.body[field]) {
                errors.push(`Field '${field}' is missing from request body`);
            }
        });

        await repo.count(req.params.id)
            .then((film) => {
                if (film['COUNT(*)'] <= 0) {
                    errors.push(`Le film ${req.params.id} n'existe pas`);
                }
            })
            .catch((err) => {
                res.status(500).json({ error: err.message });
            });

        if (req.body.genre_id) {
            await repoGenre.count(req.body.genre_id)
                .then((genre) => {
                    if (genre['COUNT(*)'] <= 0) {
                        errors.push(`Il est impossible de modifer le film puisque le genre '${req.body.genre_id}' n'existe pas`);
                    }
                })
                .catch((err) => {
                    res.status(500).json({ error: err.message });
                });
        }

        if (req.body.actor_id) {
            await Promise.all(req.body.actor_id.map(async (idActor) => {
                await repoActor.count(idActor)
                    .then((actor) => {
                        if (actor['COUNT(*)'] <= 0) {
                            errors.push(`Il est impossible de modifer le film puisque l'acteur '${idActor}' n'existe pas`);
                        }
                    })
                    .catch((err) => {
                        res.status(500).json({ error: err.message });
                    });
            }));
        }

        if (errors.length) {
            res.status(400).json({
                success: false,
                errors,
            });
            return;
        }

        let resultUpdate = {};

        await repo.update(
            req.params.id,
            {
                name: req.body.name,
                synopsis: req.body.synopsis,
                release_year: req.body.release_year,
                genre_id: req.body.genre_id,
            },
        )
            .then((result) => {
                resultUpdate = result;
            })
            .catch((err) => {
                res.status(400).json({ error: err.message });
            });

        await repoFilmActor.delete(req.params.id)
            .catch((err) => {
                res.status(400).json({ error: err.message });
            });

        await Promise.all(req.body.actor_id.map(async (idActor) => {
            await repoFilmActor.create({
                film_id: req.params.id,
                actor_id: idActor,
            })
                .catch((err) => {
                    res.status(400).json({ error: err.message });
                });
        }));

        res
            .status(201)
            .json({
                success: true,
                data: resultUpdate,
            });
    }
};

exports.film_delete = (req, res) => {
    const repo = new FilmRepository(db);
    const repoFilmActor = new FilmActorRepository(db);

    repo.delete(req.params.id)
        .then(() => {
            repoFilmActor.delete(req.params.id)
                .then(() => {
                    res.status(204)
                        .json({
                            success: true,
                        });
                })
                .catch((err) => {
                    res.status(400).json({ error: err.message });
                });
        })
        .catch((err) => {
            res.status(400).json({ error: err.message });
        });
};

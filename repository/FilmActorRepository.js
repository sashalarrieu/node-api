/* eslint-disable no-console */
/* eslint-disable func-names */
class FilmActorRepository {
    constructor(database) {
        this.database = database;
    }

    listFilm(idFilm) {
        return new Promise((resolve, reject) => {
            this.database.all('SELECT * FROM films_actors WHERE film_id = ?', [idFilm], (err, rows) => {
                if (err) {
                    console.error(err.message);
                    reject(err);
                } else {
                    resolve(rows.map((row) => this.decorator(row)));
                }
            });
        });
    }

    create(data) {
        return new Promise((resolve, reject) => {
            this.database.run(
                'INSERT INTO films_actors (film_id, actor_id) VALUES (?,?)',
                [data.film_id, data.actor_id],
                (err) => {
                    if (err) {
                        console.error(err.message);
                        reject(err);
                    } else {
                        resolve(true);
                    }
                },
            );
        });
    }

    delete(idFilm) {
        return new Promise((resolve, reject) => {
            this.database.run(
                `DELETE FROM films_actors
                 WHERE film_id = ?`,
                [idFilm],
                (err) => {
                    if (err) {
                        console.error(err.message);
                        reject(err);
                    } else {
                        resolve(true);
                    }
                },
            );
        });
    }

    // eslint-disable-next-line class-methods-use-this
    decorator(filmActor) {
        return {
            ...filmActor,
        };
    }
}

module.exports = FilmActorRepository;

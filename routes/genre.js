const express = require('express');
const genreController = require('../controllers/GenreController');

const router = express.Router();

router.get('/', genreController.genre_list);
router.get('/:id', genreController.genre_get);
router.post('/', genreController.genre_create);
router.put('/:id', genreController.genre_update);
router.delete('/:id', genreController.genre_delete);

module.exports = router;
    